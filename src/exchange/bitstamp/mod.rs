use crate::{
    model::SubKind, ExchangeId, ExchangeTransformer, MarketEvent, Subscriber, Subscription,
    SubscriptionIds, SubscriptionMeta,
};
use barter_integration::{
    error::SocketError,
    model::{InstrumentKind, SubscriptionId},
    protocol::websocket::WsMessage,
    Transformer, Validator,
};
use model::{BitstampMessage, BitstampSubResponse};
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::collections::HashMap;
use tokio::sync::mpsc;

pub mod model;

#[derive(Clone, Eq, PartialEq, Debug, Deserialize, Serialize)]
pub struct Bitstamp {
    pub ids: SubscriptionIds,
}

impl Subscriber for Bitstamp {
    type SubResponse = BitstampSubResponse;

    fn base_url() -> &'static str {
        "wss://ws.bitstamp.net/"
    }

    fn build_subscription_meta(
        subscriptions: &[Subscription],
    ) -> Result<SubscriptionMeta, SocketError> {
        // Allocate SubscriptionIds HashMap to track identifiers for each actioned Subscription
        let mut ids = SubscriptionIds(HashMap::with_capacity(subscriptions.len()));

        // Map Barter Subscriptions to Bitstamp subscriptions
        let subscriptions = subscriptions
            .iter()
            .map(|subscription| {
                let (channel, market) = Self::build_channel_meta(subscription)?;

                // Use "channel_market" as the SubscriptionId key in the SubscriptionIds
                // eg/ SubscriptionId("live_trades_btcusd")
                ids.insert(Bitstamp::subscription_id(channel, &market), subscription.clone());

                Ok(Self::subscription(channel, &market))
            })
            .collect::<Result<Vec<_>, SocketError>>()?;

        Ok(SubscriptionMeta {
            ids,
            expected_responses: subscriptions.len(),
            subscriptions,
        })
    }
}

impl ExchangeTransformer for Bitstamp {
    const EXCHANGE: ExchangeId = ExchangeId::Bitstamp;
    fn new(_: mpsc::UnboundedSender<WsMessage>, ids: SubscriptionIds) -> Self {
        Self { ids }
    }
}

impl Transformer<MarketEvent> for Bitstamp {
    type Input = BitstampMessage;
    type OutputIter = Vec<Result<MarketEvent, SocketError>>;

    fn transform(&mut self, input: Self::Input) -> Self::OutputIter {
        match input {
            BitstampMessage::Trade {
                subscription_id,
                trade,
            } => {
                let instrument = match self.ids.find_instrument(&subscription_id) {
                    Ok(instrument) => instrument,
                    Err(error) => return vec![Err(error)],
                };

                vec![Ok(MarketEvent::from((
                    Bitstamp::EXCHANGE,
                    instrument.clone(),
                    trade,
                )))]
            }
        }
    }
}

impl Bitstamp {
    pub const CHANNEL_TRADES: &'static str = "live_trades";

    pub fn build_channel_meta(sub: &Subscription) -> Result<(&str, String), SocketError> {
        let sub = sub.validate()?;

        let channel = match &sub.kind {
            SubKind::Trade => Self::CHANNEL_TRADES,
            other => {
                return Err(SocketError::Unsupported {
                    entity: Self::EXCHANGE.as_str(),
                    item: other.to_string(),
                })
            }
        };

        let market = match &sub.instrument.kind {
            InstrumentKind::Spot => format!("{}{}", sub.instrument.base, sub.instrument.quote),
            InstrumentKind::FuturePerpetual => Err(SocketError::Unsupported {
                entity: Self::EXCHANGE.as_str(),
                item: InstrumentKind::FuturePerpetual.to_string(),
            })?,
        };

        Ok((channel, market))
    }

    pub fn subscription(channel: &str, market: &str) -> WsMessage {
        WsMessage::Text(
            json!({
                "event": "bts:subscribe",
                "data": {
                    "channel": format!("{}_{}", channel, market),
                },
            })
            .to_string(),
        )
    }

    pub fn subscription_id_from_id(id: &str) -> SubscriptionId {
        SubscriptionId::from(id)
    }

    pub fn subscription_id(channel: &str, market: &str) -> SubscriptionId {
        SubscriptionId::from(format!("{channel}_{market}"))
    }
}