use crate::exchange::bitstamp::Bitstamp;
use crate::exchange::de_str;
use crate::{
    model::{DataKind, PublicTrade},
    ExchangeId, MarketEvent,
};
use barter_integration::{
    error::SocketError,
    model::{Exchange, Instrument, Side, SubscriptionId},
    Validator,
};
use chrono::{Utc, TimeZone};
use serde::{Deserialize, Serialize};

#[derive(Clone, Eq, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub struct BitstampSubResponse {
    pub event: String,
    pub channel: String,
    pub data: Data,
}

#[derive(Clone, Eq, PartialEq, Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub struct Data {
    message: Option<String>,
}

impl Validator for BitstampSubResponse {
    fn validate(self) -> Result<Self, SocketError>
    where
        Self: Sized,
    {
        match self.event.as_str() {
            "bts:subscription_succeeded" => Ok(self),
            "bts:error" => Err(SocketError::Subscribe(format!(
                "received failure subscription response: {}",
                self.data.message.unwrap()
            ))),
            _ => Err(SocketError::Subscribe(format!(
                "unknown subscription response: {}",
                self.event
            ))),
        }
    }
}

#[derive(Clone, PartialEq, Debug, Deserialize)]
#[serde(tag = "event", rename_all = "lowercase")]
pub enum BitstampMessage {
    Trade {
        #[serde(rename = "channel", deserialize_with = "de_trade_subscription_id")]
        subscription_id: SubscriptionId,
        #[serde(alias = "data")]
        trade: BitstampTrade,
    },
}

impl From<&BitstampMessage> for SubscriptionId {
    fn from(message: &BitstampMessage) -> Self {
        println!("from bitstamp message subscription id: {:?}", message);
        match message {
            BitstampMessage::Trade {
                subscription_id, ..
            } => subscription_id.clone(),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Debug, Deserialize)]
pub struct BitstampTrade {
    pub id: u64,
    #[serde(deserialize_with = "de_str")]
    pub timestamp: i64,
    pub amount: f64,
    pub price: f64,
    #[serde(alias = "type")]
    pub side: u64,
}

impl From<(ExchangeId, Instrument, BitstampTrade)> for MarketEvent {
    fn from((exchange, instrument, trade): (ExchangeId, Instrument, BitstampTrade)) -> Self {
        let side = match trade.side {
            0 => Side::Buy,
            1 => Side::Sell,
            _ => panic!("unknown side: {}", trade.side),
        };
        Self {
            exchange_time: Utc.timestamp(trade.timestamp, 0),
            received_time: Utc::now(),
            exchange: Exchange::from(exchange.as_str()),
            instrument,
            kind: DataKind::Trade(PublicTrade {
                id: trade.id.to_string(),
                price: trade.price,
                quantity: trade.amount,
                side: side,
            }),
        }
    }
}

pub fn de_trade_subscription_id<'de, D>(deserializer: D) -> Result<SubscriptionId, D::Error>
where
    D: serde::de::Deserializer<'de>,
{
    serde::de::Deserialize::deserialize(deserializer)
        .map(|id| Bitstamp::subscription_id_from_id(id))
}